package com.pakawat.affirmations.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Affirmation(
    @StringRes val stringResourceId : Int, @DrawableRes val imageResouceId: Int) //annotation เช็คว่าเป็น string , drawable จริงไหม

