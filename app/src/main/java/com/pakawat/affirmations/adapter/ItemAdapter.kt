package com.pakawat.affirmations.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pakawat.affirmations.R
import com.pakawat.affirmations.R.id.item_image
import com.pakawat.affirmations.model.Affirmation

class ItemAdapter(private val context: Context, private val dataset: List<Affirmation>) :
    RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just an Affirmation object.

    //แก้ตรงนี้
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        val textView: TextView = view.findViewById(R.id.item_title)
        val imgView: ImageView = view.findViewById(item_image)
    }


    /**
     * Create new views (invoked by the layout manager)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder { //Recycle View สร้าง holder เพื่อประหยัดทรัพยากร
        val adapterLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    /**
     * Replace the contents of a view (invoked by the layout manager)
     */
    //แก้ตรงนี้

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) { // set attribute ต่างๆ เช่น ใส่รูป ใส่ตัวหนังสือ , ใส่ event ของview ได้เช่นกัน
        val item = dataset[position]
        holder.textView.text = context.resources.getString(item.stringResourceId)
        holder.imgView.setImageResource(item.imageResouceId)
    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     */
    override fun getItemCount(): Int {
        return dataset.size
    }

}